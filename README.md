## BCR API
Rayisa Yudha Pratama

- List Car (GET) = https://challenge-8-backend.herokuapp.com/v1/cars
- Get Car (GET) = https://challenge-8-backend.herokuapp.com/v1/cars/:id
- Create Car (POST) = https://challenge-8-backend.herokuapp.com/v1/cars
- Update Car (PUT) = https://challenge-8-backend.herokuapp.com/v1/cars/:id
- Delete Car (DELETE) = https://challenge-8-backend.herokuapp.com/v1/cars/:id
- Rent Car (POST) = https://challenge-8-backend.herokuapp.com/v1/cars/:id/rent
- Login (POST) = https://challenge-8-backend.herokuapp.com/v1/auth/login
- Register (POST) = https://challenge-8-backend.herokuapp.com/v1/auth/register
- Who Am I (GET) = https://challenge-8-backend.herokuapp.com/v1/auth/whoami
